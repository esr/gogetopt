package gogetopt

import (
	"testing"
)

func assertBool(t *testing.T, see bool, expect bool) {
	t.Helper()
	if see != expect {
		t.Errorf("assertBool: expected %v saw %v", expect, see)
	}
}

func assertEqual(t *testing.T, a string, b string) {
	t.Helper()
	if a != b {
		t.Fatalf("assertEqual: expected %q == %q", a, b)
	}
}

func TestGetopt(t *testing.T) {
	var options map[string]string
	var arguments []string
	var err error

	hasOpt := func(name string) bool {
		_, ok := options[name]
		return ok
	}

	emptyArgs := func(t *testing.T) {
		if len(arguments) > 0 {
			t.Errorf("emptyArgs: arguments unexpectedly nonempty: %v", arguments)
		}
	}

	assertError := func(t *testing.T, errout bool) {
		if (err == nil) == errout {
			t.Errorf("noError: error status unexpectedly non-nil: '%v' from %s", err, options)
		}
	}

	options, arguments, err = getopt([]string{"-a"}, "ab", nil)
	assertBool(t, hasOpt("-a"), true)
	assertBool(t, hasOpt("-b"), false)
	emptyArgs(t)
	assertError(t, false)

	options, arguments, err = getopt([]string{"-c"}, "ab", nil)
	assertError(t, true)
	emptyArgs(t)

	options, arguments, err = getopt([]string{"-a", "foo", "-b"}, "a:b", nil)
	assertError(t, false)
	assertEqual(t, options["-a"], "foo")
	emptyArgs(t)

	options, arguments, err = getopt([]string{"-a", "foo", "-b", "drinkme"}, "a:b", nil)
	assertError(t, false)
	assertEqual(t, options["-a"], "foo")
	assertEqual(t, arguments[0], "drinkme")

	options, arguments, err = getopt([]string{"-a", "-b", "drinkme"}, "a:b", nil)
	assertError(t, true)

	options, arguments, err = getopt([]string{"-a", "--sample"}, "ab", []string{"--sample"})
	assertBool(t, hasOpt("--sample"), true)
	assertError(t, false)

	options, arguments, err = getopt([]string{"-a", "--sample=bar"}, "ab", []string{"--sample="})
	assertBool(t, hasOpt("--sample"), true)
	assertEqual(t, options["--sample"], "bar")
	assertError(t, false)
}
