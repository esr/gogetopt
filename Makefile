#
# Makefile for gogetopt
#

build:
	go build

test:
	go test ./...

vet:
	go vet ./...

lint:
	golint ./...

check: vet lint test

format:
	go fmt ./...


